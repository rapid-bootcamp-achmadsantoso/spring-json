package com.rapidtech.springjson.service.impl;

import com.rapidtech.springjson.entity.AddressEntity;
import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.repository.AddressRepo;
import com.rapidtech.springjson.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {
    private AddressRepo repo;

    @Autowired
    public AddressServiceImpl(AddressRepo repo){
        this.repo=repo;
    }
    @Override
    public Optional<AddressModel> save(AddressModel model) {
        if(model == null) {
            return Optional.empty();
        }
        AddressEntity entity = new AddressEntity(model);
        try {
            this.repo.save(entity);
            return Optional.of(new AddressModel(entity));
        }catch (Exception e){
            log.error("Address save is failed, error: {}", e.getMessage());
            return Optional.empty();
        }
    }
}
