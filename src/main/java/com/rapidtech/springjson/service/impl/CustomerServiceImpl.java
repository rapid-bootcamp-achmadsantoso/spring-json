package com.rapidtech.springjson.service.impl;

import com.rapidtech.springjson.entity.AddressEntity;
import com.rapidtech.springjson.entity.CustomerEntity;
import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.CustomerRequest;
import com.rapidtech.springjson.model.CustomerResponse;
import com.rapidtech.springjson.repository.CustomerRepo;
import com.rapidtech.springjson.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepo repo;
    @Autowired
    public CustomerServiceImpl(CustomerRepo repo){
        this.repo=repo;
    }

    @Override
    public CustomerResponse saveAll(CustomerRequest request) {
        if(request.getCustomers().isEmpty()){
            return new CustomerResponse();
        }
        CustomerResponse response = new CustomerResponse();
        int countSuccess = 0;
        int countFailed = 0;

        List<CustomerModel> customerModels = new ArrayList<>();
        for(CustomerModel model: request.getCustomers()){
            //panggil method save yang ada dibawah
            Optional<CustomerModel> customerModel = this.save(model);
            //check datanya
            if(customerModel.isPresent()){
                customerModels.add(model);
                countSuccess++;
            }else {
                countFailed++;
            }
        }
        return new CustomerResponse(customerModels, countSuccess, countFailed);
//        response.setData(customerModels);
//        response.setSuccessSave(countSuccess);
//        response.setFailedSave(countFailed);
//        return response;

    }

    @Override
    public Optional<CustomerModel> save(CustomerModel model) {
        if(model == null) {
            return Optional.empty();
        }
        CustomerEntity entity = new CustomerEntity(model);
        try {
            this.repo.save(entity);
            return Optional.of(new CustomerModel(entity));
        }catch (Exception e){
            log.error("Customer save is failed, error: {}", e.getMessage());
            return Optional.empty();
        }
    }
}
