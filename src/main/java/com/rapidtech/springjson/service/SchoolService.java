package com.rapidtech.springjson.service;

import com.rapidtech.springjson.model.SchoolModel;

import java.util.Optional;

public interface SchoolService {
    Optional<SchoolModel> save(SchoolModel model);
}
