package com.rapidtech.springjson.service;

import antlr.collections.List;
import com.rapidtech.springjson.model.AddressModel;

import java.util.Optional;

public interface AddressService {
Optional<AddressModel> save(AddressModel model);
}
