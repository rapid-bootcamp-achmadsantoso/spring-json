package com.rapidtech.springjson.model;

import com.rapidtech.springjson.entity.CustomerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerModel implements Serializable {
    private Long id;
    private String fullName;
    private List<AddressModel> address;
    private String gender;
    private Date dateOfBirth;
    private String placeOfBirth;
    private List<SchoolModel> schools;

    public CustomerModel(CustomerEntity entity) {
        BeanUtils.copyProperties(entity, this);
    }


}
