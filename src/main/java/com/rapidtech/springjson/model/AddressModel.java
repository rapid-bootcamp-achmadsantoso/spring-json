package com.rapidtech.springjson.model;

import com.rapidtech.springjson.entity.AddressEntity;
import com.rapidtech.springjson.entity.CustomerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressModel {
    private String name;
    private String address;
    private String village;
    private String district;
    private String city;
    private String province;




    public AddressModel(AddressEntity entity) {
        BeanUtils.copyProperties(entity, this);
    }

}
