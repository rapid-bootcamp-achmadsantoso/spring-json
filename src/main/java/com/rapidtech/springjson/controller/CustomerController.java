package com.rapidtech.springjson.controller;

import com.rapidtech.springjson.model.CustomerRequest;
import com.rapidtech.springjson.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class CustomerController  {
    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service){
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Object> saveCustomer(@RequestBody CustomerRequest request){

        return ResponseEntity.ok().body(
                service.saveAll(request)
        );
    }
}
