package com.rapidtech.springjson.entity;

import com.rapidtech.springjson.model.CustomerModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer_tab")
public class CustomerEntity {
    @Id
    @TableGenerator(name =  "customer_id", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue = "customer_id", initialValue = 0, allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "customer_id")
    private Long id;

    @Column(name = "customer_full_name", length = 255, nullable = false)
    private String fullName;

    @Column(name = "customer_gender", length = 255, nullable = false)
    private String gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "customer_date_of_birth", nullable = false)
    private Date dateOfBirth;

    @Column(name = "customer_place_of_birth", length = 255, nullable = false)
    private String placeOfBirth;


    @OneToMany(mappedBy = "customer")
    private Set<AddressEntity> address = new HashSet<>();


    @OneToMany(mappedBy = "customer")
    private Set<SchoolEntity> schools = new HashSet<>();

    public CustomerEntity(CustomerModel model) {
        BeanUtils.copyProperties(model, this);
    }


}
