package com.rapidtech.springjson.entity;

import com.rapidtech.springjson.model.AddressModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name= "address_tab")
public class AddressEntity {
    @Id
    @TableGenerator(name =  "address_id", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue = "address_id", initialValue = 0, allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "address_id")
    private Long id;

    @Column(name = "address_name", length = 255, nullable = false)
    private String name;

    @Column(name = "address_detail", length = 255, nullable = false)
    private String address;

    @Column(name = "address_village", length = 255, nullable = false)
    private String village;

    @Column(name = "address_district", length = 255, nullable = false)
    private String district;

    @Column(name = "address_city",length = 255, nullable = false)
    private  String city;

    @Column(name = "address_province", length = 255, nullable = false)
    private String province;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="customer_id", insertable = false, updatable = false)
    private CustomerEntity customer;

    public AddressEntity(AddressModel model){
        BeanUtils.copyProperties(model, this);
    }

}
